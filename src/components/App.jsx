import React from 'react';
import ReactDOM from 'react-dom';
import Loader from 'react-loader-spinner'
import AppNavbar from './Navbar';
import Board from './Board';
import Container from "react-bootstrap/Container";
import {connect} from "react-redux";

import './App.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import 'bootstrap/dist/css/bootstrap.min.css';

import {fillStore} from "../redux/actions";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        };
    }

    fetchData() {
        return new Promise((resolve) => {
            fetch('/boardData').then((data) => {
                return data.json();
            }).then((json) => {
                setTimeout(() => {
                    resolve(json);
                }, 500)

            });
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

    }

    componentDidMount() {
        this.fetchData().then((data) => {
            this.props.fillStore(data);
            this.setState({
                loading: false
            });
        });
    }

    getBoardData(boardId) {
        const currentBoardData = this.props.data.boards[boardId];
        currentBoardData.listsData = currentBoardData.lists.reduce((acc, listId) => {
            acc[listId] = this.props.data.lists[listId];
            return acc;
        }, {});

        currentBoardData.tasksData = currentBoardData.lists.reduce((acc, listId) => {
            currentBoardData.listsData[listId].tasks.forEach((taskId) => {
                acc[taskId] = this.props.data.tasks[taskId];
            });
            return acc;
        }, {});

        return currentBoardData;
    }

    render() {
        return (
            <React.Fragment>
                {this.state.loading ? (
                    <Loader type="Plane"
                            color="#00BFFF"/>) : (
                    <React.Fragment>
                        <AppNavbar data={{
                            boards: this.props.data.boards,
                            selectedBoard: this.props.selectedBoard,
                            needSave: this.props.needSave
                        }}/>
                        <Board data={this.getBoardData(this.props.selectedBoard)}/>
                    </React.Fragment>
                )}

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    debugger;
    return {
        ...state
    }
};

export default connect(
    mapStateToProps,
    {fillStore}
)(App);