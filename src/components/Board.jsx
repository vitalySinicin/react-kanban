import React from 'react';
import ReactDOM from 'react-dom';
import CardContainer from './CardContainer';
import {DragDropContext, Droppable} from "react-beautiful-dnd";
import {taskReplace, listReplace} from "../redux/actions";
import {connect} from "react-redux";

import './Board.css';

class Board extends React.Component {
    constructor(props) {
        super(props);
    }

    listMoved(source, destination) {
        this.props.listReplace(source.index, destination.index);
    }

    taskMoved(source, destination, draggableId) {
        this.props.taskReplace(source.droppableId, destination.droppableId, draggableId, source.index, destination.index);
    }

    onDragEnd(result) {
        const {destination, source, draggableId} = result;
        if (!destination) {
            return;
        }
        if (destination.droppableId === source.droppableId && destination.index === source.index) {
            return;
        }

        switch (result.type) {
            case "list":
                this.listMoved(source, destination);
                break;
            case "task":
                this.taskMoved(source, destination, draggableId);
                break;
            default :
                break;
        }
    }

    getListData(listId) {
        const currentListData = this.props.data.listsData[listId];

        currentListData.tasksData = currentListData.tasks.reduce((acc, taskId) => {
            acc[taskId] = this.props.data.tasksData[taskId];
            return acc;
        }, {});

        return currentListData;
    }

    render() {
        return <DragDropContext onDragEnd={this.onDragEnd.bind(this)}>
            <Droppable droppableId={'board'} direction='horizontal' type='list'>
                {(provided) => (
                    <div id='board' ref={provided.innerRef} {...provided.droppableProps}>
                        {this.props.data.lists.map((listId, index) => {
                            return <CardContainer key={listId} index={index} data={this.getListData(listId)}/>;
                        })}
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        </DragDropContext>
    }
}

export default connect(
    null,
    {taskReplace, listReplace}
)(Board);