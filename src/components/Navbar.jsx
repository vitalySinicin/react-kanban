import React from 'react';
import ReactDOM from 'react-dom';
import './Navbar.css';
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import {FaPlusCircle} from 'react-icons/fa';
import {IoIosSave} from "react-icons/io";

import {connect} from "react-redux";
import {listAdd, saveBoard} from "../redux/actions";

class AppNavbar extends React.Component {
    constructor(props) {
        super(props);
    };

    addList = () => {
        this.props.listAdd('New column');
    };

    async saveBoard () {
        debugger;
        let result  =  await this.saveData();
        debugger;

        this.props.saveBoard(this.props.data.selectedBoard);
        console.log('ss');

    };
    async saveData (){
        let response = await fetch('/saveBoard',{
             method: 'POST',
             headers: {
                 'Content-Type': 'application/json;charset=utf-8'
             },
             body: ''
         });

        debugger;

        let responseText = await response.text();
            return responseText;

    }

    getBoardDataById(boardId) {
        return this.props.data.boards[boardId];
    }

    render() {
        return <Navbar bg="light" >
            <Navbar.Brand>React-Kanban</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <NavDropdown title={this.getBoardDataById(this.props.data.selectedBoard).name}
                                 id="basic-nav-dropdown">
                        {Object.keys(this.props.data.boards).map((boardId) => {
                            return <NavDropdown.Item
                                key={boardId}>{this.props.data.boards[boardId].name}</NavDropdown.Item>
                        })}
                        <NavDropdown.Item
                            key="add_board">
                            <FaPlusCircle id="add_list_btn"
                                          style={{
                                              color: 'green',
                                              cursor: 'pointer',
                                              marginRight: '5px'
                                          }}>

                            </FaPlusCircle>
                            Create a new board
                        </NavDropdown.Item>
                    </NavDropdown>
                    <Button variant="outline-info"
                            disabled={!this.props.data.needSave}
                            onClick={this.saveBoard.bind(this)}>
                        <IoIosSave/>
                    </Button>
                </Nav>
                <Form inline>
                    <Button onClick={this.addList.bind(this)} variant="outline-success">
                        <FaPlusCircle id="add_list_btn"
                                      style={{
                                          color: 'green',
                                          cursor: 'pointer',
                                          marginRight: '5px'
                                      }}/>
                        Create a list
                    </Button>

                </Form>
            </Navbar.Collapse>
        </Navbar>

    }
}

export default connect(
    null,
    {listAdd, saveBoard}
)(AppNavbar);