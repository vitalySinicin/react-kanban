import React from 'react';
import ReactDOM from 'react-dom';
import Card from "./Card";
import {Droppable, Draggable} from "react-beautiful-dnd";
import {connect} from "react-redux";
import EditableTitle from "./EditableTitle";
import {taskAdd, removeList, listRename} from "../redux/actions";
import {FaPlus} from 'react-icons/fa';
import {FaTrashAlt} from 'react-icons/fa';
import NavDropdown from "react-bootstrap/NavDropdown";

import './CardContainer.css';

class CardContainer extends React.Component {
    constructor(props) {
        super(props);
    };

    addTask = () => {
        this.props.taskAdd(this.props.data.id, 'New Task', '');
    };

    removeList = () => {
        this.props.removeList(this.props.data.id);
    };

    drawCards = (tasks) => {
        return tasks.map((taskId, index) => (
            <Card
                key={taskId}
                index={index}
                data={this.props.data.tasksData[taskId]}
                parentId={this.props.data.id}/>));
    };

    changeTitle = (newValue) => {
        this.props.listRename(this.props.data.id, newValue);
    };

    drawHeader(title) {
        return <div className='card_container_header'>
            <EditableTitle value={title} valueChanged={this.changeTitle.bind(this)}/>
            <div className='header_control_buttons'>
                <div className='header_control_buttons_remove' onClick={this.removeList}>
                    <FaTrashAlt/>
                </div>
            </div>
        </div>
    }

    drawBody(tasks) {
        return (provided) => (<div className='card_container_body' ref={provided.innerRef} {...provided.droppableProps}>
            {this.drawCards(tasks)}
            {provided.placeholder}
        </div>)
    }

    render() {
        return <Draggable draggableId={this.props.data.id} index={this.props.index}>
            {(provided) => (
                <div className='card_container_wrapper'
                     ref={provided.innerRef}  {...provided.draggableProps} {...provided.dragHandleProps}>
                    <div className='card_container'>
                        {this.drawHeader(this.props.data.name)}
                        <Droppable droppableId={this.props.data.id} type='task'>
                            {this.drawBody(this.props.data.tasks)}
                        </Droppable>
                        <div className="card_add_btn" onClick={this.addTask}><FaPlus/></div>
                    </div>
                </div>)}
        </Draggable>
    }
}

export default connect(
    null,
    {taskAdd, removeList, listRename}
)(CardContainer);