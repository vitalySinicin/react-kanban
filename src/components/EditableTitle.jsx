import React from 'react';
import ReactDOM from 'react-dom';

import './EditableTitle.css';


class EditableTitle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editMode: false,
            value: ''
        }
        this.textBoxRef = React.createRef();
    }

    componentDidUpdate(prevProps, prevState) {
        const textBoxElement = this.textBoxRef.current;
        if (textBoxElement && prevState.editMode !== this.state.editMode) {
            textBoxElement.setSelectionRange(0, textBoxElement.value.length);
        }
    }

    swithEditMode = () => {
        if (this.state.editMode) {
            this.props.valueChanged && this.props.valueChanged(this.state.value); //When closing edit mode
        }
        this.setState({
            ...this.state,
            editMode: !this.state.editMode,
            value: this.props.value
        });
    };

    handleChange = (e) => {
        this.setState({
            ...this.state,
            value: e.target.value
        });
    };

    handleKeyDown = (e) => {
        if (e.keyCode === 13) {// Pressed Enter
            this.swithEditMode();
        }
    };

    handleBlur = (e) => {
        this.swithEditMode();
    };

    render() {
        return (
            <div className='editable_title' onDoubleClick={this.swithEditMode}>
                {this.state.editMode ? (
                    <input type="text"
                           className="editable_title__input"
                           autoFocus
                           value={this.state.value}
                           onChange={this.handleChange}
                           onKeyDown={this.handleKeyDown}
                           onBlur={this.handleBlur}
                           ref={this.textBoxRef}
                    />
                ) : (
                    <span className='editable_title_value' title={this.props.value}>{this.props.value}</span>
                )}
            </div>
        );
    }
}

export default EditableTitle;