import React from 'react';
import ReactDOM from 'react-dom';
import EditableTitle from "./EditableTitle";
import {IoIosRemoveCircleOutline} from 'react-icons/io';
import {FaEdit, FaExclamation} from "react-icons/fa";
import {TiMinus} from "react-icons/ti";
import {connect} from "react-redux";
import {Draggable} from "react-beautiful-dnd";
import {taskRemove, taskRename} from "../redux/actions";

import './Card.css';

class AppCard extends React.Component {
    constructor(props) {
        super(props);
    }

    removeTask() {
        this.props.taskRemove(this.props.parentId, this.props.data.id);
    }

    drawBody(content) {
        return <div className='card_body'>
            <span className='card_content'>{content}</span>
        </div>
    }

    changeTitle = (newValue) => {
        this.props.taskRename(this.props.data.id, newValue);
    };

    drawHeader(title) {
        return <div className='card_header'>
            <div className='card_header_status'>
                {this.props.data.important && <FaExclamation/>}
            </div>
            <EditableTitle value={title} valueChanged={this.changeTitle.bind(this)}/>
            <div className='card_header_control_buttons'>
                <div className='card_header_control_buttons_edit'>
                    <FaEdit/>
                </div>
                <div className='card_header_control_buttons_remove' onClick={this.removeTask.bind(this)}>
                    <TiMinus/>
                </div>
            </div>
        </div>
    }

    render() {
        return <Draggable draggableId={this.props.data.id} index={this.props.index}>
            {(provided) => (
                <div className='task_card'
                     ref={provided.innerRef}  {...provided.draggableProps} {...provided.dragHandleProps}>
                    {this.drawHeader(this.props.data.name)}
                    {this.drawBody(this.props.data.content)}
                </div>
            )}

        </Draggable>
    }
}

const CardComponent = connect(
    null,
    {taskRemove, taskRename}
)(AppCard);


export default CardComponent;