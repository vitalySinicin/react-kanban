export const FILL_STORE = "FILL_STORE";

export const BOARD_SAVE = "BOARD_SAVE";

export const TASK_ADD = "TASK_ADD";
export const TASK_REMOVE = "TASK_REMOVE";
export const TASK_REPLACE = "TASK_REPLACE";
export const TASK_RENAME = "TASK_RENAME";

export const LIST_ADD = "LIST_ADD";
export const LIST_REMOVE = "LIST_REMOVE";
export const LIST_REPLACE = "LIST_REPLACE";
export const LIST_RENAME = "LIST_RENAME";

