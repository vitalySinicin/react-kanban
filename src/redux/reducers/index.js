import * as _ from "lodash"

import {
    TASK_ADD,
    TASK_REMOVE,
    TASK_REPLACE,
    LIST_ADD,
    LIST_REMOVE,
    LIST_REPLACE,
    FILL_STORE,
    LIST_RENAME,
    TASK_RENAME,
    BOARD_SAVE
} from "../actionTypes";

const initialState = {
    needSave: false,
    selectedBoard: "399bcfdf-782a-4017-b9a2-d40ba361d731",
    data: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        //STORE
        case FILL_STORE: {
            return {
                ...state,
                data: {...action.payload.data}
            };
        }
        //BOARD
        case BOARD_SAVE: {
            debugger;
            return {
                ...state,
                needSave: false
            };
        }
        //TASKS
        case TASK_ADD: {
            const taskId = action.payload.id;
            const taskName = action.payload.name;
            const taskContent = action.payload.content;
            const listId = action.payload.listId;

            const stateCopy = _.cloneDeep(state);

            stateCopy.data.tasks[taskId] = {
                id: taskId,
                name: taskName,
                important: false,
                content: taskContent
            };

            stateCopy.data.lists[listId].tasks.push(taskId);
            stateCopy.needSave = true;

            return stateCopy;
        }
        case TASK_REMOVE: {
            const taskId = action.payload.id;
            const listId = action.payload.listId;
            const stateCopy = _.cloneDeep(state);

            stateCopy.data.tasks = _.omit(stateCopy.data.tasks, [taskId]);
            _.remove(stateCopy.data.lists[listId].tasks, (id) => id === taskId);
            stateCopy.needSave = true;

            return stateCopy;
        }
        case TASK_REPLACE: {
            const stateCopy = _.cloneDeep(state);
            const taskId = action.payload.id;
            const listFromId = action.payload.fromId;
            const listToId = action.payload.toId;
            const fromIndex = action.payload.fromIndex;
            const toIndex = action.payload.toIndex;

            const movedTask = stateCopy.data.lists[listFromId].tasks.splice(fromIndex, 1);

            if (listFromId === listToId) { //only sorting
                stateCopy.data.lists[listFromId].tasks.splice(toIndex, 0, ...movedTask);
            } else {
                stateCopy.data.lists[listToId].tasks.splice(toIndex, 0, ...movedTask);
            }
            stateCopy.needSave = true;

            return stateCopy;
        }
        case TASK_RENAME: {
            const taskId = action.payload.id;
            const taskName = action.payload.name;
            const stateCopy = _.cloneDeep(state);

            stateCopy.data.tasks[taskId].name = taskName;
            stateCopy.needSave = true;

            return stateCopy;
        }

        //LISTS
        case LIST_ADD: {
            const stateCopy = _.cloneDeep(state);
            const listId = action.payload.id;

            stateCopy.data.lists[action.payload.id] = {
                id: listId,
                name: action.payload.name,
                tasks: []
            };
            stateCopy.data.boards[stateCopy.selectedBoard].lists.push(listId);
            stateCopy.needSave = true;

            return stateCopy;
        }
        case LIST_REMOVE: {
            const listId = action.payload.id;
            const stateCopy = _.cloneDeep(state);
            const tasksForDelete = stateCopy.data.lists[listId].tasks;
            stateCopy.data.tasks = _.omit(stateCopy.data.tasks, ...tasksForDelete);
            stateCopy.data.lists = _.omit(stateCopy.data.lists, [listId]);
            _.remove(stateCopy.data.boards[stateCopy.selectedBoard].lists, (id) => id === listId);
            stateCopy.needSave = true;

            return stateCopy;
        }
        case LIST_REPLACE: {
            const fromIndex = action.payload.fromIndex;
            const toIndex = action.payload.toIndex;
            const stateCopy = _.cloneDeep(state);

            const movedList = stateCopy.data.boards[stateCopy.selectedBoard].lists.splice(fromIndex, 1);
            stateCopy.data.boards[stateCopy.selectedBoard].lists.splice(toIndex, 0, ...movedList);
            stateCopy.needSave = true;

            return stateCopy;
        }
        case LIST_RENAME: {
            const listId = action.payload.id;
            const listName = action.payload.name;
            const stateCopy = _.cloneDeep(state);

            stateCopy.data.lists[listId].name = listName;
            stateCopy.needSave = true;

            return stateCopy;
        }

        default: {
            return state;
        }
    }
};
