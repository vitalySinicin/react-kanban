import uuid from 'react-uuid';
import {
    FILL_STORE,
    LIST_ADD,
    LIST_REMOVE,
    TASK_ADD,
    TASK_REMOVE,
    TASK_REPLACE,
    LIST_REPLACE,
    LIST_RENAME,
    TASK_RENAME,
    BOARD_SAVE
} from "./actionTypes";

export const fillStore = (data) => ({
    type: FILL_STORE,
    payload: {
        data
    }
});
export const saveBoard = (data) => ({
    type: BOARD_SAVE,
    payload: {
        data
    }
});

export const listAdd = (name) => ({
    type: LIST_ADD,
    payload: {
        id: uuid(),
        name,
        cards: []
    }
});

export const removeList = (id) => ({
    type: LIST_REMOVE,
    payload: {
        id
    }
});

export const listReplace = (fromIndex, toIndex) => ({
    type: LIST_REPLACE,
    payload: {
        fromIndex,
        toIndex
    }
});
export const listRename = (id, name) => ({
    type: LIST_RENAME,
    payload: {
        id,
        name
    }
});

export const taskAdd = (listId, name, content) => ({
    type: TASK_ADD,
    payload: {
        id: uuid(),
        listId,
        name,
        content
    }
});

export const taskRemove = (listId, taskId) => ({
    type: TASK_REMOVE,
    payload: {
        id: taskId,
        listId
    }
});

export const taskReplace = (fromId, toId, taskId, fromIndex, toIndex) => ({
    type: TASK_REPLACE,
    payload: {
        id: taskId,
        fromId,
        toId,
        fromIndex,
        toIndex
    }
});

export const taskRename = (id, name) => ({
    type: TASK_RENAME,
    payload: {
        id,
        name
    }
});

