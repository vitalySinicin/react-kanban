import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import store from "./redux/store";
const rootEl = document.getElementById('root');
import App from './components/App';

store.subscribe(() => {
    console.log(store.getState());
});

const renderApp = () => {
    ReactDOM.render(
        <Provider store={store}>
            <App/>
        </Provider>,
        rootEl
    );
};

renderApp();

// This checks for local changes and automatically refreshes the browser (hot-reloading)
if (module.hot) {
    module.hot.accept('./src/index.jsx', () => renderApp());
}