const fs = require('fs');
const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');

const app = express();
const config = require('./webpack.config.js');
const compiler = webpack(config);

// Tell express to use the webpack-dev-middleware and use the webpack.config.js
// configuration file as a base.
app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath,
}));

app.get('/boardData', function (req, res) {
    fs.readFile('Data.json', (err, data) => {
        if (err) throw err;
        let jsonData = JSON.parse(data);
        res.json(jsonData);
    });

});
app.post('/saveBoard', function (req, res) {
    console.log(222222);
  res.send('ok');

});

// Serve the files on port 3000.
app.listen(3000, function () {
    console.log('Example app listening on port 3000!\n');
});